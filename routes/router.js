const express = require('express')
const router = express.Router()
const db = require('../db/dbConnection')
const { signupValidation, loginValidation } = require('../validations/validation')
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')
const { validationResult } = require('express-validator');

const JWT_SECRET = 'shixiaofeng'

// 注册
router.post('/register', signupValidation, (req, res, next) => {
    // 表单字段验证
    const errors = validationResult(req)
    if(!errors.isEmpty()){
        return res.status(422).json({errors:errors.array()})
    }

    let sql = `select * from users where lower(email) = lower(${db.escape(req.body.email)})`
    db.query(sql, (err, result) => {
        if (result.length) {
            return res.status(409).send({
                msg: '邮箱已被注册'
            })
        } else {
            bcrypt.hash(req.body.password, 10, (err, hash) => {
                if (err) {
                    return res.status(500).send({
                        msg: err
                    })
                } else {
                    let insertSql = `insert into users (name,email,password) values('${req.body.name}',${db.escape(req.body.email)},${db.escape(hash)})`
                    db.query(insertSql, (err, result) => {
                        if (err) {
                            return res.status(400).send({
                                msg: err
                            })
                        }
                        return res.status(200).send({
                            msg: '用户注册成功'
                        })
                    })
                }
            })
        }
    })
})

// 登录
router.post('/login', loginValidation, (req, res, next) => {
    // 表单字段验证
    const errors = validationResult(req)
    if(!errors.isEmpty()){
        return res.status(422).json({errors:errors.array()})
    }

    let searchSql = `select * from users where email = ${db.escape(req.body.email)}`
    db.query(searchSql, (err, result) => {
        if (err) {
            return res.status(400).send({
                msg: err
            })
        }

        if (!result.length) {
            return res.status(401).send({
                msg: '用户名或密码错误'
            })
        }

        bcrypt.compare(
            req.body.password, result[0]['password'], (bErr, bResult) => {
                if (bErr) {
                    return res.status(401).send({
                        msg: '用户名或密码错误'
                    })
                }
                if (bResult) {
                    // 生成token
                    const token = jwt.sign({ id: result[0].id }, JWT_SECRET, { expiresIn: '1h' })
                    // 记录最后登录时间
                    db.query(`update users set last_login = now() where id='${result[0].id}'`)
                    return res.status(200).send({
                        msg: '登录成功',
                        token,
                        user: result[0]
                    })
                }
                return res.status(401).send({
                    msg: '用户名或密码错误'
                })
            }
        )
    })
})

// 获取用户信息
router.post('/get-user', (req, res, next) => {
    if (
        !req.headers.authorization ||
        !req.headers.authorization.startsWith('Bearer') ||
        !req.headers.authorization.split(' ')[1]
    ) {
        return res.status(422).send({
            message: '缺少token'
        })
    }

    const theToken = req.headers.authorization.split(' ')[1];
    const decoded = jwt.verify(theToken, JWT_SECRET);
    db.query(`select * from users where id=?`, decoded.id, function (error, results, fields) {
        if (error) throw error;
        return res.send({ error: false, data: results[0], message: '请求成功' })
    })
})


module.exports = router;