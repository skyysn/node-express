const express = require('express');
// 解析post请求体的数据
const bodyParser = require('body-parser');
// 允许跨域的请求访问
const cors = require('cors');
const indexRouter = require('./routes/router.js');

const app = express();

app.use(express.json());

app.use(bodyParser.json());

app.use(bodyParser.urlencoded({extended:true}));

app.use(cors());

app.use('/api',indexRouter);

// 处理错误
app.use((err,req,res,next)=>{
    err.statusCode = err.statusCode || 500;
    err.message = err.message || "Intrenal Server Error";
    res.status(err.statusCode).json({
        message:err.message
    })
})

//  监听3000端口
app.listen(3000,() => console.log(`服务启动成功：http://localhost:3000`))