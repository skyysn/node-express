# node-express

#### 介绍
node.js + express + mysql + jwt 实现用户管理restful api

#### 软件架构
express
启动基本的http服务

express-validator
用来验证表单是否合法

mysql
连接mysql数据库，存储数据，查询数据

body-parser
解析post请求体的数据

jsonwebtoken
用于生成jwt的token

bcryptjs
加密用户注册的密码

cors
允许跨域的请求访问

#### 安装教程

1.  npm install
2.  mysql 表
    CREATE TABLE `users` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `name` varchar(50) NOT NULL,
    `email` varchar(50) NOT NULL,
    `password` varchar(200) NOT NULL,
    PRIMARY KEY (id),
    UNIQUE KEY email (email)
    ) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;


#### 使用说明

1.  npm start

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
